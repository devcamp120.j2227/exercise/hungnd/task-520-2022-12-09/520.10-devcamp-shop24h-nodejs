const productModel = require("../model/productModel");

//function tạo product mới
const createProduct = (req, res) => {
    const body = req.body;

    if (!body.Id) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Id Product không hợp lệ"
        })
    }
    if (!body.Name) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name Product không hợp lệ"
        })
    }
    if (!body.Type) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Type Product không hợp lệ"
        })
    }
    if (!body.ImageUrl) {
        return res.status(400).json({
            status: "Bad Request",
            message: "ImageUrl Product không hợp lệ"
        })
    }
    if (!body.BuyPrice) {
        return res.status(400).json({
            status: "Bad Request",
            message: "BuyPrice Product không hợp lệ"
        })
    }
    if (!body.PromotionPrice) {
        return res.status(400).json({
            status: "Bad Request",
            message: "PromotionPrice Product không hợp lệ"
        })
    }

    const newProduct = {
        Id: body.Id,
        Name: body.Name,
        Type: body.Type,
        ImageUrl: body.ImageUrl,
        BuyPrice: body.BuyPrice,
        PromotionPrice: body.PromotionPrice,
        Description: body.Description
    }
    productModel.create(newProduct, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(201).json({
            message: "Create new product successfully",
            Product: data
        })
    })
}
//tạo function get product
const getAllProduct = (req, res) => {
    productModel
        .find()
        .limit(8)
        .exec((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: "Get all Products successfully",
            data: data
        })
    })
}
// get product by id
const getProductById = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const productId = req.params.productId;
    //B2: validate dữ liệu
    if(!productId){
        return res.status(400).json({
            status: "Bad request",
            message: "product Id không hợp lệ"
        })
    }
    //B3: gọi model tìm dữ liệu
    productModel.findById(productId, (error, data) =>{
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status:" Get Product by id successfully",
            data: data
        })
    })
}
// update product
const updateProductById = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const productId = req.params.productId;
    const body = req.body;

    //B2: validate dữ liệu
    if(!productId){
        return res.status(400).json({
            status: "Bad request",
            message: "product Id không hợp lệ"
        })
    }
    if (!body.Name) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name Product không hợp lệ"
        })
    }
    if (!body.Type) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Type Product không hợp lệ"
        })
    }
    if (!body.ImageUrl) {
        return res.status(400).json({
            status: "Bad Request",
            message: "ImageUrl Product không hợp lệ"
        })
    }
    if (!body.BuyPrice || isNaN(body.BuyPrice)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "BuyPrice Product không hợp lệ"
        })
    }
    if (!body.PromotionPrice || isNaN(body.PromotionPrice)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "PromotionPrice Product không hợp lệ"
        })
    }
    //B3: gọi model update dữ liệu
    const updateProduct = {};
    if(body.Name !== undefined){
        updateProduct.Name = body.Name
    }
    if(body.Type !== undefined){
        updateProduct.Type = body.Type
    }
    if(body.ImageUrl !== undefined){
        updateProduct.ImageUrl = body.ImageUrl
    }
    if(body.BuyPrice !== undefined){
        updateProduct.BuyPrice = body.BuyPrice
    }
    if(body.PromotionPrice !== undefined){
        updateProduct.PromotionPrice = body.PromotionPrice
    }
    if(body.Description !== undefined){
        updateProduct.Description = body.Description
    }

    productModel.findByIdAndUpdate(productId, updateProduct, (error, data) =>{
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: "Update product successfully",
            data: data
        })
    })

}
// Delete Product
const deleteProductById =  (req, res) => {
    //B1: chuẩn bị dữ liệu
    const productId = req.params.productId;
    //B2: validate dữ liệu
    if(!productId){
        return res.status(400).json({
            status: "Bad request",
            message: "product Id không hợp lệ"
        })
    }
    //B3: gọi model xóa dữ liệu 
    productModel.findByIdAndDelete(productId,(error, data) => {
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: "Deleted product successfullly"
        })
    })
}
module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProductById,
    deleteProductById
};
