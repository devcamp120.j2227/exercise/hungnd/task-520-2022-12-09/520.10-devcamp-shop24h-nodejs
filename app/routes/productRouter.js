//Khai báo thư viện express js
const express = require("express");

//khai báo router app
const router = express.Router();

//Import Product controller
const productController = require("../controllers/productController");

router.post("/products", productController.createProduct);
router.get("/products", productController.getAllProduct);
router.get("/products/:productId", productController.getProductById);
router.put("/products/:productId", productController.updateProductById);
router.delete("/products/:productId", productController.deleteProductById);

module.exports = router;